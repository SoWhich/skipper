(local iter (require :skipper.iter))


(assert (= (-> 2
    (iter.repeat)
    (iter.take 12)
    (iter.sum)) 24))

