(local luv (require :skipper.luv))
(import-macros {: guard : <!} :skipper)

(local timer (luv.timer 100))
(guard [loop (luv.start)]
       :uv/timer nil (print "timer done!"))
