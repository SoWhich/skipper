(import-macros {: guard : <!} :skipper)

(var total 0)
(guard [gen (do
              (<! :iter 1)
              (<! :iter 2)
              (<! :iter 3)
              (<! :iter 4)
              (<! :iter 5)
              (<! :iter 6))]
       :iter v (do (set total (+ total v)) (gen)))

(assert (= total 21))
