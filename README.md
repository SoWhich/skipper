# Skipper

A fennel algebraic-effects helper macro thingy based vaguely off [this
paper](https://logic.cs.tsukuba.ac.jp/~sat/pdf/tfp2020-slide.pdf).

Eventually I'd like to make a neat little wrapper library around libuv and maybe
Glib/GObject, but currently I only have a skeleton around what libuv would look
like and it's broken 🙃.

TODO: Small writeup on thoughts on fennel and writing this/writing a lisp macro
