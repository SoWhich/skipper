(import-macros {: guard : <!} :skipper)

(fn flatten [gen]
  (fn []
    (guard [cont (gen)]
      :iter box (guard [nested (box)]
                       :iter term (<! :iter term)))))

(fn flatmap [gen fmapf]
  (-> gen
    (map fmapf)
    (flatten)))

(fn repeat [value]
  (fn rh [] (<! :iter value)
  (rh value)))

(fn take [gen count]
  (fn []
    (var count count)
    (if (= count 0) nil
    (guard [cont (gen)]
      :iter val (do (<! :iter val)
                  (set count (- count 1))
                  (if (= count 0) nil (cont)))))))

(fn filter [gen pred]
  (fn [] (guard [cont (gen)]
    :iter val (do
                (if (pred val) (<! :iter val))
                (cont)))))

(fn map [gen mapf]
  (fn [] (guard [cont (gen)]
    :iter val (do
                (<! :iter (mapf val))
                (cont)))))

(fn fold [gen init op]
  (var acc init)
  (guard [cont (gen)]
    :iter v (do (set acc (op acc v)) (cont)))
  acc)

(fn ints []
  (fn helper [last]
    (<! :iter (last))
    (helper (+ last 1))
  (fn [] (helper 0))))

(fn sum [gen]
  (fold gen 0 (fn [a b] (+ a b))))

{: filter
 : flatmap
 : flatten
 : fold
 : ints
 : map
 : repeat
 : sum
 : take}
