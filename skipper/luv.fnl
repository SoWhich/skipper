(local uv (require :luv))
(import-macros {: guard : <!} :skipper)

(fn timer [timeout repeat ...]
  (local rest (table.pack ...))
  (local timer (uv.new_timer))
  (timer:start timeout (or repeat 0) (fn [] (<! :uv/timer (table.unpack rest))))
  timer)



{: timer
 :start uv.run}
