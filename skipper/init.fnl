
(fn compile-time-string? [sym]
  (and (= (tostring sym) sym)))

(fn pprint-table [toPrint]
  (table.concat (icollect [_ v (ipairs toPrint)] (view v)) " "))

(fn <! [effect ...]
  (assert-compile
    (compile-time-string? effect)
    "effect name should be a compile time string like :yield or \"iter\""
    effect)
  `(coroutine.yield {(require :skipper.tag) [,effect ,...]}))

(fn guard-helper [mechanism resumer expr ...]
  (fn unpack-or-nest [collection]
    (if
      (sequence? collection) collection
      (list? collection) (table.unpack collection))
      collection)
  (local errhandlers [])
  (local returners [])
  (local handlers [])
  (var intermediaries [])

  (each [_ elem (ipairs (table.pack ...))]
    (case intermediaries
      ; error in coroutine guard
      (where [effect pattern] (= (tostring effect) "&err"))
        (do
           (table.insert errhandlers pattern)
           (table.insert errhandlers elem)
           (set intermediaries []))

      ; completed return guard
      (where [effect pattern] (= (tostring effect) "&end"))
        (do
           (table.insert returners pattern)
           (table.insert returners elem)
           (set intermediaries []))

      ; handler interceptor
      (where [effect pattern] (= (tostring effect) "&snag"))
        (do
           (table.insert handlers pattern)
           (table.insert handlers elem)
           (set intermediaries []))

      ; completed return guard
      [effect pattern]
        (do (assert-compile
          (compile-time-string? effect)
          "effect name should be a compile time string like :yield or \"err\""
          effect)
           (table.insert handlers [effect (unpack-or-nest pattern)])
           (table.insert handlers elem)
           (set intermediaries []))

      [effect]
        (table.insert intermediaries elem)

      []
        (table.insert intermediaries elem)))

  (assert-compile
    (= (length intermediaries) 0)
    (.. "Missing piece(s?) of guard clause(s?), left with "
        (pprint-table intermediaries))
    intermediaries)

  ; propagate unhandled effects
  (table.insert handlers '_#)
  (table.insert handlers '(coroutine.yield value#))

  ; return any not explicitly guarded return values
  (table.insert returners 'ret#)
  (table.insert returners 'ret#)

  ; propogate any unhandled errors
  (table.insert errhandlers 'err#)
  (table.insert errhandlers '(error err#))

  `(let [coro# (coroutine.create (fn [] ,expr))]

    ; enables mutually recursive local functions
    (var ,resumer nil)
    (var dispatch# nil)

    (set ,resumer (fn [ ... ]
        (let [(boolstate# value#) (coroutine.resume coro# ...)]
          (dispatch# boolstate# value#))))

    (set dispatch# (fn [boolstate# value#]
              (if
                (not boolstate#)
                ; this hack with unpack is not my favorite, but it's the
                ; easiest way I know to splice this table unwrapped
                ; within another list
                  (,mechanism value# ,(unpack errhandlers))

                (= :dead (coroutine.status coro#))
                  (,mechanism value# ,(unpack returners))

                ; propogate non-skipper coroutine suspensions
                (= (?. value# (require :skipper.tag)) nil)
                  (coroutine.yield value#)

                (,mechanism (. value# (require :skipper.tag))
                  ,(unpack handlers)))))
    (,resumer)))

(fn guard [[resumer expr] ...]
  (guard-helper 'case resumer expr ...))

(fn guard-match [[resumer expr] ...]
  (guard-helper 'match resumer expr ...))

{: guard : guard-match : <!}
